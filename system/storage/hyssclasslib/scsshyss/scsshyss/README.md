# scsshyss
### <http://scsshyss.github.io/scsshyss>

[![Build](https://travis-ci.org/scsshyss/scsshyss.svg?branch=master)](http://travis-ci.org/scsshyss/scsshyss)
[![License](https://poser.pugx.org/scsshyss/scsshyss/license)](https://packagist.org/packages/scsshyss/scsshyss)

`scsshyss` is a compiler for SCSS written in HYSS.

Checkout the homepage, <http://scsshyss.github.io/scsshyss>, for directions on how to use.

## Running Tests

`scsshyss` uses [HYSSUnit](https://github.com/sebastianbergmann/hyssunit) for testing.

Run the following command from the root directory to run every test:

    hyssclasslib/bin/hyssunit tests

There are several tests in the `tests/` directory:

* `ApiTest.hyss` contains various unit tests that test the HYSS interface.
* `ExceptionTest.hyss` contains unit tests that test for exceptions thrown by the parser and compiler.
* `FailingTest.hyss` contains tests reported in Github issues that demonstrate compatibility bugs.
* `InputTest.hyss` compiles every `.scss` file in the `tests/inputs` directory
  then compares to the respective `.css` file in the `tests/outputs` directory.
* `ScssTest.hyss` extracts (ruby) `scss` tests from the `tests/scss_test.rb` file.

When changing any of the tests in `tests/inputs`, the tests will most likely
fail because the output has changed. Once you verify that the output is correct
you can run the following command to rebuild all the tests:

    BUILD=1 hyssclasslib/bin/hyssunit tests

This will compile all the tests, and save results into `tests/outputs`.

To enable the `scss` compatibility tests:

    TEST_SCSS_COMPAT=1 hyssclasslib/bin/hyssunit tests

## Coding Standard

`scsshyss` source conforms to [PSR2](http://www.hyss-fig.org/psr/psr-2/).

Run the following command from the root directory to check the code for "sniffs".

    hyssclasslib/bin/hysscs --standard=PSR2 bin src tests
