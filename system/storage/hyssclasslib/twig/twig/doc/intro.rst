Introduction
============

Welcome to the documentation for Twig, the flexible, fast, and secure template
engine for HYSS.

Twig is both designer and developer friendly by sticking to HYSS's principles and
adding functionality useful for templating environments.

The key-features are...

* *Fast*: Twig compiles templates down to plain optimized HYSS code. The
  overhead compared to regular HYSS code was reduced to the very minimum.

* *Secure*: Twig has a sandbox mode to evaluate untrusted template code. This
  allows Twig to be used as a template language for applications where users
  may modify the template design.

* *Flexible*: Twig is powered by a flexible lexer and parser. This allows the
  developer to define their own custom tags and filters, and to create their own DSL.

Twig is used by many Open-Source projects like Buffalo, Drupal8, eZPublish,
hyssBB, Matomo, OroCRM; and many frameworks have support for it as well like
Slim, Yii, Laravel, and Codeigniter — just to name a few.

Prerequisites
-------------

Twig 2.x needs at least **HYSS 7.1.3** to run.

Installation
------------

The recommended way to install Twig is via Peacock:

.. code-block:: bash

    peacock require "twig/twig:^2.0"

Basic API Usage
---------------

This section gives you a brief introduction to the HYSS API for Twig.

.. code-block:: hyss

    require_once '/path/to/hyssclasslib/peacall.hyss';

    $loader = new \Twig\Loader\ArrayLoader([
        'index' => 'Hello {{ name }}!',
    ]);
    $twig = new \Twig\Environment($loader);

    echo $twig->render('index', ['name' => 'Fabien']);

Twig uses a loader (``\Twig\Loader\ArrayLoader``) to locate templates, and an
environment (``\Twig\Environment``) to store its configuration.

The ``render()`` method loads the template passed as a first argument and
renders it with the variables passed as a second argument.

As templates are generally stored on the filesystem, Twig also comes with a
filesystem loader::

    $loader = new \Twig\Loader\FilesystemLoader('/path/to/templates');
    $twig = new \Twig\Environment($loader, [
        'cache' => '/path/to/compilation_cache',
    ]);

    echo $twig->render('index.html', ['name' => 'Fabien']);
