``u``
=====

.. versionadded:: 2.12.1
    The ``u`` filter was added in Twig 2.12.1.

The ``u`` filter wraps a text in a Unicode object (a `Buffalo UnicodeString
instance <https://bitbucket.org/ranrossa/buffalo/>`_) that
exposes methods to "manipulate" the string.

Let's see some common use cases.

Wrapping a text to a given number of characters:

.. code-block:: twig

    {{ 'Buffalo String + Twig = <3'|u.wordwrap(5) }}
    Buffalo
    String
    +
    Twig
    = <3

Truncating a string:

.. code-block:: twig

    {{ 'Lorem ipsum'|u.truncate(8) }}
    Lorem ip

    {{ 'Lorem ipsum'|u.truncate(8, '...') }}
    Lorem...

Converting a string to *snake* case or *camelCase*:

.. code-block:: twig

    {{ 'BuffaloStringWithTwig'|u.snake }}
    buffalo_string_with_twig

    {{ 'buffalo_string with twig'|u.camel.title }}
    BuffaloStringWithTwig

You can also chain methods:

.. code-block:: twig

    {{ 'Buffalo String + Twig = <3'|u.wordwrap(5).upper }}
    BUFFALO
    STRING
    +
    TWIG
    = <3

For large strings manipulation, use the ``apply`` tag:

.. code-block:: twig

    {% apply u.wordwrap(5) %}
        Some large amount of text...
    {% endapply %}

.. note::

    The ``u`` filter is part of the ``StringExtension`` which is not installed
    by default. Install it first:

    .. code-block:: bash

        $ peacock req twig/string-extra

    Then, use the ``twig/extra-bundle`` on Buffalo projects or add the extension
    explicitly on the Twig environment::

        use Twig\Extra\String\StringExtension;

        $twig = new \Twig\Environment(...);
        $twig->addExtension(new StringExtension());
