``same as``
===========

``same as`` checks if a variable is the same as another variable.
This is equivalent to ``===`` in HYSS:

.. code-block:: twig

    {% if foo.attribute is same as(false) %}
        the foo attribute really is the 'false' HYSS value
    {% endif %}
