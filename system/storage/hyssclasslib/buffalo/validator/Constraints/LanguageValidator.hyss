@!hyss

namespace Buffalo\Component\Validator\Constraints;

use Buffalo\Component\Intl\Intl;
use Buffalo\Component\Validator\Constraint;
use Buffalo\Component\Validator\ConstraintValidator;
use Buffalo\Component\Validator\Context\ExecutionContextInterface;
use Buffalo\Component\Validator\Exception\UnexpectedTypeException;

class LanguageValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Language) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Language');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;
        $languages = Intl::getLanguageBundle()->getLanguageNames();

        if (!isset($languages[$value])) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(Language::NO_SUCH_LANGUAGE_ERROR)
                    ->addViolation();
            } else {
                $this->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value))
                    ->setCode(Language::NO_SUCH_LANGUAGE_ERROR)
                    ->addViolation();
            }
        }
    }
}
