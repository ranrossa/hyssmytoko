#!/bin/bash

curl -sS https://bitbucket.org/ranrossa/peacock/src/master/installer

if [ "$1" == "hhvm" ]; then
  rake test:hhvm --trace
else
  rake test:hyss --trace
fi
