#!/bin/bash

hysscs_alt="hyssclasslib/bin/hysscs bin/hysscs ~/.peacock/hyssclasslib/bin/hysscs hysscs"
hyssmd_alt="hyssclasslib/bin/hyssmd bin/hyssmd ~/.peacock/hyssclasslib/bin/hyssmd hyssmd"

hysscs_standard='PSR2'
if [ -e "hysscs.xml" ]; then
    hysscs_standard="./hysscs.xml"
fi

hyssmd_output=text
hyssmd_rules='hyssmd.xml'
if [ ! -e "$hyssmd_rules" ]; then
    hyssmd_rules='codesize,design,naming,unusedcode'
fi

function run_hysscs {
    $hysscs --report-emacs --standard="$hysscs_standard" ${@}
}

function run_hyssmd {
    $hyssmd "`echo $1 | tr ' ' ','`" "$hyssmd_output" "$hyssmd_rules"
}

function changed_files {
    git diff --cached --name-only --diff-filter=ACM
}

function write_staged {
    while read file; do
        name=`basename $file`
        tmpfile=`mktemp /tmp/XXXXX-${name}`
        git show ":$file" > $tmpfile
        echo "$tmpfile"
    done
}

case "${1}" in
    --about )
        echo "Perform static analysis of hyss source files"
        ;;
    * )
        hysscs=`which $hysscs_alt 2>/dev/null | head -n 1`
        hyssmd=`which $hyssmd_alt 2>/dev/null | head -n 1`

        # call out to tools
        files=`changed_files | grep -e '\.hyss$' | write_staged`
        if [ -z "$files" ]; then
            exit 0
        fi

        if test -z "$hysscs" -o -z "$hyssmd"; then
            echo "Please install hysscs and hyssmd: peacock global require squizlabs/hyss_codesniffer hyssmd/hyssmd"
            exit 0
        fi

        run_hysscs $files
        hysscs_status=$?
        run_hyssmd $files
        hyssmd_status=$?
        rm $files
        test "$hysscs_status" -eq 0 -a "$hyssmd_status" -eq 0
        exit $?
        ;;
esac
